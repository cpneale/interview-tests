﻿using System;
using System.Text;

namespace Ogi.InterviewChallenges.NetDev
{
    public static partial class StaticTestMethods
    {
        public static string ReverseStringWithBuilder(string originalString)
        {
            if (originalString == null)
                throw new ArgumentNullException(nameof(originalString));

            StringBuilder sb = new StringBuilder(originalString.Length - 1);

            for (int i = originalString.Length - 1; i >= 0; i--)
            {
                sb.Append(originalString[i]);
            }

            return sb.ToString();
        }

        public static string ReverseStringByHalf(string originalString)
        {
            if (!IsNullCheck(originalString))
            {
                char[] inputAsArray = originalString.ToCharArray();
                int inputLength = originalString.Length;
                for(int i = 0; i < (inputLength/2); i++)
                {
                    char temp = inputAsArray[i];
                    inputAsArray[i] = inputAsArray[inputLength - (i + 1)];
                    inputAsArray[inputLength - (i + 1)] = temp;
                }
                return new string(inputAsArray);
            }

            return string.Empty;
        }

        public static bool IsNullCheck(string originalString)
        {
            if (originalString == null)
                throw new ArgumentNullException(nameof(originalString));
            else
                return false;
        }
    }
}

